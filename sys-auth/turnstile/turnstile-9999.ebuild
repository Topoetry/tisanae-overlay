# Copyright 2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson

DESCRIPTION="Independent session/login tracker"
HOMEPAGE="https://github.com/chimera-linux/turnstile"
if [[ ${PV} != *9999* ]]; then
	RESTRICT="mirror"
	SRC_URI="https://github.com/chimera-linux/turnstile/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64"
else
	inherit git-r3
	EGIT_REPO_URI="https://github.com/chimera-linux/turnstile"
fi

LICENSE="BSD-2"
SLOT="0"
IUSE="-runit"

DEPEND="sys-libs/pam
		runit? ( sys-process/runit )"

src_configure() {
	# idk how to package dinit support cleanly so
	local emesonargs=(
		-Ddinit=disabled
		-Dlibrary=enabled
		$(meson_feature runit)
	)
	meson_src_configure
}

src_install() {
	meson_install
	newinitd "${FILESDIR}/turnstiled.initd" turnstiled
	newconfd "${FILESDIR}/turnstiled.confd" turnstiled
}
