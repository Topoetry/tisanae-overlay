# Tisanae, my personnal gentoo overlay
`
* media-fonts/nerd-fonts-symbols
     Available versions:  (~)3.1.1 {X mono +wide}
     Homepage:            https://github.com/ryanoasis/nerd-fonts
     Description:         High number of extra glyphs from popular 'iconic fonts'

* media-plugins/distrho-ports
     Available versions:  **9999*l^m (~)20210315^m{gpkg} {lv2 vst}
     Homepage:            https://github.com/DISTRHO/DISTRHO-Ports
     Description:         Linux ports of Distrho plugins

* media-sound/bslizr
     Available versions:  (~)1.2.14 (~)1.2.16{gpkg} **9999*l {uwu}
     Homepage:            https://github.com/sjaehn/BSlizr
     Description:         Sequenced audio slicing effect LV2 plugin

* media-sound/carla
     Available versions:  (~)2.5.4^m **9999-r1*l {X alsa gtk gtk2 opengl osc (-)pulseaudio rdf sf2 sndfile PYTHON_SINGLE_TARGET="python3_10 python3_11"}
     Homepage:            http://kxstudio.linuxaudio.org/Applications:Carla
     Description:         Fully-featured audio plugin host, supports many audio drivers and plugin formats

* media-sound/cyanrip
     Available versions:  (~)0.9.0^m (~)0.9.2^m **9999*l
     Homepage:            https://github.com/cyanreg/cyanrip
     Description:         Bule-ish CD ripper

* media-sound/drumgizmo
     Available versions:  [M](~)0.9.19 [M](~)0.9.20 {(+)alsa jack lv2 vst CPU_FLAGS_X86="sse sse2 sse3"}
     Homepage:            https://www.drumgizmo.org/wiki
     Description:         Cross-platform drum plugin and stand-alone application

* media-sound/geonkick
     Available versions:  (~)3.3.2^m{gpkg} **9999*l
     Homepage:            https://geonkick.org/
     Description:         A free software percussion synthesizer

* media-sound/iempluginsuite
     Available versions:  [M]~1.13.0 {jack standalone vst2 vst3}
     Homepage:            https://git.iem.at/audioplugins/IEMPluginSuite
     Description:         The IEM Plug-in Suite is a free and Open-Source audio plug-in suite.

* media-sound/surge
     Available versions:  1.8.1*l 1.9.0*l{gpkg} {lv2 vst vst3}
     Homepage:            https://surge-synthesizer.github.io//
     Description:         Surge is an open source digital synthesizer

* media-sound/zita-at1
     Available versions:  (~)0.6.2^m {pkgconf}
     Homepage:            http://kokkinizita.linuxaudio.org/linuxaudio/
     Description:         An autotuner, normally used to correct the pitch of a voice singing (slightly) out of tune

* media-sound/zita-rev1
     Available versions:  (~)0.2.2^m {pkgconf}
     Homepage:            http://kokkinizita.linuxaudio.org/linuxaudio/
     Description:         Zita-rev1 is a reworked version of the reverb originally developed for Aeolus

* media-video/harvid
     Available versions:  (~)0.9.0 (~)0.9.1
     Homepage:            https://x42.github.io/harvid/
     Description:         HTTP Ardour Video Daemon

* net-misc/urlview
     Available versions:  (~)0.9_p21{gpkg:2}
     Homepage:            https://packages.qa.debian.org/u/urlview.html
     Description:         A curses URL parser for text files

* sys-power/fanctl
     Available versions:  ~0.6.3 ~0.6.4^m {debug doc}
     Homepage:            https://gitlab.com/mcoffin/fanctl
     Description:         Fancontrol replacement in Rust with YAML configuration
`
