# Copyright 1999-2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

# package versioning is bullshit so i converted to hex
MY_PV_MAIN="$(printf '%2x' ${PV/\.*/})"
MY_PV="${MY_PV_MAIN}-${PV/*\./}"
DESCRIPTION="A curses URL parser for text files"
SLOT="0"
HOMEPAGE="https://git.sr.ht/~nabijaczleweli/urlview.deb"
SRC_URI="https://git.sr.ht/~nabijaczleweli/urlview.deb/archive/debian/${MY_PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2+"
KEYWORDS="~amd64"

RDEPEND="sys-libs/ncurses:0"
DEPEND="${RDEPEND}"
S="${WORKDIR}/${PN}.deb-debian/${MY_PV}"
PATCHES="${FILESDIR}/makefile.patch"
