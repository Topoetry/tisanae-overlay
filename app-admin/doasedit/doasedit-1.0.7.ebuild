# Copyright 1999-2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

RESTRICT="mirror"
DESCRIPTION="Enable doers to edit non-user-editable files with an unprivileged editor"

HOMEPAGE="https://codeberg.org/TotallyLeGIT/doasedit"

SRC_URI="https://codeberg.org/TotallyLeGIT/doasedit/archive/${PV}.tar.gz -> ${P}.tar.gz"

S="${WORKDIR}/${PN}"

LICENSE="MIT"

SLOT="0"

KEYWORDS="~amd64"

RDEPEND="app-admin/doas
app-alternatives/sh"

src_configure() {
	return 0
}

src_compile() {
	return 0
}

src_install() {
	dobin doasedit
}
