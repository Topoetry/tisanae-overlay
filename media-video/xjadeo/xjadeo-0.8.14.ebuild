# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

# inherit lists eclasses to inherit functions from. For example, an ebuild
# that needs the eautoreconf function from autotools.eclass won't work
# without the following line:
inherit autotools
#
# Eclasses tend to list descriptions of how to use their functions properly.
# Take a look at the eclass/ directory for more examples.

DESCRIPTION="X JAck viDEo mOnitor: a tool that displays a video clip in sync with an external time source."

HOMEPAGE="https://xjadeo.sourceforge.net/"

SRC_URI="https://github.com/x42/xjadeo/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

# Source directory; the dir where the sources can be found (automatically
# unpacked) inside ${WORKDIR}.  The default value for S is ${WORKDIR}/${P}
# If you don't need to change it, leave the S= line out of the ebuild
# to keep it tidy.
#S="${WORKDIR}/${P}"

LICENSE="GPL-2"

SLOT="0"

KEYWORDS="~amd64"

IUSE="alsamidi freetype imlib2 ltc midi opengl osc portmidi xv"

RDEPEND=">=media-video/ffmpeg-1.0.0
alsamidi? ( media-libs/alsa-lib )
freetype? ( media-libs/freetype )
imlib2? (  media-libs/imlib2 )
ltc? ( media-libs/libltc )
opengl? (  virtual/glu )
osc? ( media-libs/liblo )
portmidi? ( media-libs/portmidi )
virtual/jack
x11-libs/libX11
xv? (  x11-libs/libXv )"

DEPEND="${RDEPEND}"

src_prepare() {
	default
	eautoreconf
}

src_configure() {
	local myeconfargs=(
		--disable-sdl
		$(use_enable alsamidi)
		$(use_enable freetype ft)
		$(use_enable imlib2)
		$(use_enable ltc)
		$(use_enable midi)
		$(use_enable opengl)
		$(use_enable osc)
		$(use_enable portmidi)
		$(use_enable xv)
	)
	econf "${myeconfargs[@]}"
}
