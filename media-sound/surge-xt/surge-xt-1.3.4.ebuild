# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake git-r3

DESCRIPTION="Surge is an open source digital synthesizer"
HOMEPAGE="https://surge-synthesizer.github.io//"
EGIT_REPO_URI="https://github.com/surge-synthesizer/surge.git"
EGIT_COMMIT="release_xt_${PV}"
LICENSE="GPL-3"
SLOT="0"
IUSE="clap lv2 standalone vst3"
KEYWORDS="amd64"
#I Shall fix dependencies all of them are already installed on my machine Im lazy
DEPEND="x11-libs/cairo
		x11-libs/libxkbcommon
		x11-libs/libxcb"
RDEPEND=${DEPEND}
PATCHES=(
	"${FILESDIR}"/errno.patch
	"${FILESDIR}/check-for-libc-backtrace.patch"
)

src_configure() {
	local mycmakeargs=(
		-DSURGE_BUILD_LV2=$(usex lv2 TRUE FALSE)
		-DSURGE_BUILD_CLAP=$(usex clap TRUE FALSE)
		-DSURGE_SKIP_VST3=$(usex !vst3 TRUE FALSE)
		-DSURGE_SKIP_STANDALONE=$(usex !standalone TRUE FALSE)
	)
	cmake_src_configure
}
