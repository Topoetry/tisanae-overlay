# Copyright 2022-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{10..12} )
inherit meson gnome2-utils

if [[ ${PV} != *9999* ]]; then
	RESTRICT="mirror"
	SRC_URI="https://gitlab.com/coderkun/mcg/-/archive/v${PV}/mcg-v${PV}.tar.bz2 -> ${P}.tar.bz2"
	KEYWORDS="~amd64"
else
	inherit git-r3
	EGIT_REPO_URI="https://gitlab.com/coderkun/mcg"
fi
DESCRIPTION="Client for the Music Player Daemon, focusing on albums instead of single tracks."
HOMEPAGE="https://www.suruatoel.xyz/codes/mcg"

LICENSE="LGPL-2"
SLOT="0"
IUSE="avahi keyring"

DEPEND=">=x11-libs/gtk+-3.22
		dev-python/pygobject
		dev-python/python-dateutil
		avahi? ( net-dns/avahi )
		keyring? ( dev-python/keyring
					dev-python/keyrings-alt )
"
RDEPEND="${DEPEND}
		>=media-sound/mpd-0.21.0
"
S="${WORKDIR}/mcg-v${PV}"

pkg_postinst() {
	gnome2_schemas_update
}
pkg_postrm() {
	gnome2_schemas_update
}
