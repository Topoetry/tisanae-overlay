# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake xdg

if [[ ${PV} != *9999* ]]; then
	RESTRICT="mirror"
	SRC_URI="https://github.com/Geonkick-Synthesizer/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
	#SRC_URI="https://codeberg.org/Geonkick-Synthesizer/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	#SRC_URI="https://gitlab.com/Geonkick-Synthesizer/${PN}/-/archive/v${PV}/${PN}-v{PV}.tar.bz2 -> ${P}.tar.bz2"
	KEYWORDS="~amd64"
else
	inherit git-r3
	EGIT_REPO_URI="https://gitlab.com/Geonkick-Synthesizer/${PN}"
fi

DESCRIPTION="A free software percussion synthesizer"
HOMEPAGE="https://geonkick.org/"
LICENSE="GPL-3"
SLOT="0"

DEPEND="
	dev-libs/rapidjson
	media-libs/libsndfile
	media-libs/lv2
"
RDEPEND="${DEPEND}"
BDEPEND="
	>=dev-build/cmake-3.7
"
