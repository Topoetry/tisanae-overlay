# Copyright 2022-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson

if [[ ${PV} != *9999* ]]; then
	RESTRICT="mirror"
	SRC_URI="https://github.com/cyanreg/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64"
else
	inherit git-r3
	EGIT_REPO_URI="https://github.com/cyanreg/${PN}"
fi
DESCRIPTION="Bule-ish CD ripper"
HOMEPAGE="https://github.com/cyanreg/cyanrip"

LICENSE="LGPL-2"
SLOT="0"

DEPEND=">=media-video/ffmpeg-4.0
		dev-libs/libcdio-paranoia
		>=media-libs/musicbrainz-5
		net-misc/curl
"
RDEPEND="${DEPEND}"
